let app = {
    skillsData: [],
    projectsData: [],

    init: function(){
        this.loadSkillsData();
        this.loadProjectsData();
    },
    loadSkillsData: function(){
        $.post('/skills')
            .done(function(response){
                response = JSON.parse(response);
                if(typeof response.error !== 'undefined'){
                    alert(response.error.code);
                }else{

                }
            });
    },
    loadProjectsData: function (page= 1, skillId = '') {
        $.post('/projects', {page: page, skillId: skillId})
            .done(function(response){
                response = JSON.parse(response);
                if(typeof response.error !== 'undefined'){
                    alert(response.error.code);
                }else{

                }
            });
    }


}

$(document).ready(function(){





});