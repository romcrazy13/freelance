<?php

define('ROOT_PATH', realpath(__DIR__ . '/..'));

require ROOT_PATH . '/vendor/autoload.php';

use Dotenv\Dotenv;
use Pecee\SimpleRouter\SimpleRouter as Router;

$dotenv = Dotenv::createUnsafeImmutable(ROOT_PATH);
$dotenv->load();

require ROOT_PATH . '/routes/web.php';

try{
    Router::start();
}catch (Exception $e){
    return $e->getMessage();
}
