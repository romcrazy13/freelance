<?php

namespace App\Services;

use App\Entities\ProjectEntity;
use App\Services\ApiClients\FreelanceApiClientService;
use Exception;

class ProjectsService
{

    private FreelanceApiClientService $apiClient;

    public function __construct()
    {
        $this->apiClient = new FreelanceApiClientService();
    }

    /**
     * @param string $skillId
     * @param int $page
     * @return object
     * @throws Exception
     */
    public function getProjectsList(string $skillId, int $page): object
    {
        $response = $this->apiClient->getProjectsList($skillId, $page);

        if($error = $response->error ?? false){
            throw new Exception($error->title, $error->status);
        }

        $projects = [];
        foreach ($response->data ?? [] as $source){
            $project = (new ProjectEntity($source))->toArray();
            $projects[] = (object)$project;
        }

        return (object)[
            'data' => $projects,
            'links' => $response->links ?? null
        ];
    }
}