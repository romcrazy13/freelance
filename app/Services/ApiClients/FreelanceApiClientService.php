<?php

namespace App\Services\ApiClients;

use App\Services\LangService;

class FreelanceApiClientService extends AbstractBaseApiClientService
{

    public function __construct()
    {
        $this->headers = [
            'Accept-Language: ' . LangService::getCurrentLang(),
            'Authorization: Bearer ' . getenv('FREELANCE_HUNT_TOKEN', '')
        ];
    }

    protected function getApiPath(): string
    {
        return getenv('FREELANCE_HUNT_API', '');
    }

    /**
     * @return object
     */
    public function getSkillsList(): object
    {
        $result = $this->request(self::METHOD_GET, '/skills');

        return (object)$result;
    }

    /**
     * @param string $skillId
     * @param int $page
     * @return array|false
     */
    public function getProjectsList(string $skillId, int $page = 1)
    {
        $params = ['page[number]' => $page];
        if($skillId){
            $params['filter[skill_id]'] = $skillId;
        }

        return $this->request(self::METHOD_GET, '/projects', $params);
    }

}