<?php

namespace App\Services\ApiClients;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractBaseApiClientService
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';

    protected array $headers = [];

    abstract protected function getApiPath(): string;

    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @return false|array
     */
    protected function request(string $method = self::METHOD_GET, string $url = '', array $params = [])
    {
        $apiPath = $this->getApiPath();

        if($method === self::METHOD_GET && !empty($params)){
            $url .= '?' . http_build_query($params);
        }

        $curl = curl_init();

        $options = [
            CURLOPT_URL => $apiPath . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
        ];
        if(!empty($this->headers)){
            $options[CURLOPT_HTTPHEADER] = $this->headers;
        }

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);

        curl_close($curl);

        return !$response
            ? trigger_error(curl_error($curl))
            : json_decode($response);
    }
}