<?php

namespace App\Services\ApiClients;

use Exception;
//use Symfony\Component\Cache\CacheItem;

class PrivateApiClientService extends AbstractBaseApiClientService
{

    private static array $rates = [];

    /**
     * @throws Exception
     */
    public function __construct()
    {
        if(empty(self::$rates)){
            self::$rates = $this->setRates();
        }
    }

    protected function getApiPath(): string
    {
        return getenv('PRIVATE_API');
    }

    /**
     * @return array
     * @throws Exception
     */
    private function setRates(): array
    {
        /**
         * todo Доделать кэширование запроса
         */
//        $cacheKey = 'private-courses';
//        /** @var CacheItem $cacheData */
//        $cacheData = $this->cache->getItem($cacheKey);

//        if ($cacheData->isHit() && $cacheData->get()) {
//            return $cacheData->get();
//        }
//        $cacheData->expiresAfter(DateInterval::createFromDateString('1 hour'));

        $result = $this->request();

//        $cacheData->set($response->getData());
//        $this->cache->save($cacheData);

        $rates['UAH'] = 1;
        foreach ($result as $item){
            $curr = $item->ccy === 'RUR' ? 'RUB' : $item->ccy;
            $rates[$curr] = ((float)$item->buy + (float)$item->sale) / 2;
        }

        return $rates;
    }

    /**
     * @return array
     */
    public function getRates(): array
    {
        return self::$rates;
    }
}