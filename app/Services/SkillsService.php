<?php

namespace App\Services;

use App\Entities\SkillEntity;
use App\Services\ApiClients\FreelanceApiClientService;

class SkillsService
{
    private FreelanceApiClientService $apiClient;

    public function __construct()
    {
        $this->apiClient = new FreelanceApiClientService();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getSkillsList(): array
    {
        $response = $this->apiClient->getSkillsList();

        if($error = $response->error ?? false){
            throw new \Exception($error->title, $error->status);
        }

        $result = [];
        foreach ($response->data as $source){
            $skill = (new SkillEntity($source))->toArray();
            $result[] = (object)$skill;
        }

        return $result;
    }
}