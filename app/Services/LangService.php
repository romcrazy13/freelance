<?php

namespace App\Services;

class LangService
{
    const LANG_RU = 'ru';
    const LANG_UK = 'uk';
    const LANG_EN = 'en';

    const LANGS_SUPPORTED = [
        self::LANG_RU,
        self::LANG_UK,
        self::LANG_EN
    ];

    private static string $currentLang;

    /**
     * @param string $currentLang
     */
    public static function setCurrentLang(string $currentLang): void
    {
        if(in_array($currentLang, self::LANGS_SUPPORTED)){
            self::$currentLang = $currentLang;
        }
    }

    /**
     * @return string
     */
    public static function getCurrentLang(): string
    {
        return self::$currentLang;
    }
}