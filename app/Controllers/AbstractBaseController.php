<?php

namespace App\Controllers;

use App\Services\LangService;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractBaseController
{

    protected ?Request $request = null;

    protected array $requestParams = [];

    public function __construct()
    {
        $this->request = Request::createFromGlobals();

        $this->requestParams = $this->collectingRequestParams();

        $lang = explode(',', $this->request->headers->get('Accept-Language', getenv('APP_DEFAULT_LANG')))[0];
        LangService::setCurrentLang($lang);
    }

    private function collectingRequestParams() : array
    {
        $result = $this->request->request->all();

        $queryString = $this->request->getQueryString();
        if(!empty($queryString)){
            parse_str($queryString, $queryParams);
            $result = array_merge($result, $queryParams);
        }

        return $result;
    }
}