<?php

namespace App\Controllers;

use App\Services\ApiClients\FreelanceApiClientService;
use App\Services\ProjectsService;
use App\Services\SkillsService;
use Exception;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractBaseController
{

    /**
     * @throws Exception
     */
    public function getSkills()
    {
        $result = (new SkillsService())->getSkillsList();

        return json_encode($result);
//        return new JsonResponse($result);
    }

    /**
     * @throws Exception
     */
    public function getProjects()
    {
        $skillId = $this->request->get('skillId', '');
        $page = $this->request->get('page', 1);

        $result = (new ProjectsService())->getProjectsList($skillId, $page);

        return json_encode($result);
//        return new JsonResponse($result);
    }

}