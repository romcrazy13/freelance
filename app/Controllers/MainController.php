<?php

namespace App\Controllers;

class MainController extends AbstractBaseController
{

    public function mainPage()
    {
        return file_get_contents(ROOT_PATH . '/views/template.html');
    }

}