<?php

namespace App\Entities;

abstract class AbstractBaseEntity
{

    protected object $source;

    public function __construct(object $source = null)
    {
        if($source){
            $this->source = $source;
            $this->sourceToEntityConvert($source);
        }
    }

    abstract protected function sourceToEntityConvert(object $source): self;

    abstract public function toArray(): array;
}