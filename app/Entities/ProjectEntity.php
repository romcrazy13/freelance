<?php


namespace App\Entities;


use App\Services\ApiClients\PrivateApiClientService;
use App\Services\LangService;

class ProjectEntity extends AbstractBaseEntity
{

    private int $id;

    private string $project_name_ru;

    private string $project_name_uk;

    private string $project_name_en;

    private string $project_link;

    private ?int $budget_amount;

    private ?string $budget_currency;

    private string $employer_name;

    private string $employer_login;

    protected function sourceToEntityConvert(object $source): self
    {
        $this->id = $source->id;
        $this->{'project_name_' . LangService::getCurrentLang()} = $source->attributes->name;
        $this->project_link = $source->links->self->web;
        $this->budget_amount = $source->attributes->budget->amount ?? null;
        $this->budget_currency = $source->attributes->budget->currency ?? null;
        $this->employer_name = $source->attributes->employer->first_name;
        $this->employer_login = $source->attributes->employer->login;

        return $this;
    }

    public function toArray(): array
    {
        $rates = (new PrivateApiClientService())->getRates();

        $budget = (empty($this->budget_amount))
        ? null
        : ($this->budget_amount * $rates[$this->budget_currency]);

        return [
            'id' => $this->id,
            'project_name' => $this->{'project_name_' . LangService::getCurrentLang()},
            'project_link' => $this->project_link,
            'budget' => !empty($budget) ? $budget . ' UAH' : null,
            'employer_name' => $this->employer_name,
            'employer_login' => $this->employer_login,
        ];
    }
}