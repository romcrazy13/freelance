<?php

namespace App\Entities;

use App\Services\LangService;

class SkillEntity extends AbstractBaseEntity
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name_ru;

    /**
     * @ORM\Column(type="string")
     */
    private string $name_uk;

    /**
     * @ORM\Column(type="string")
     */
    private string $name_en;

    protected function sourceToEntityConvert(object $source): self
    {
        $this->id = $source->id;
        $this->{'name_' . LangService::getCurrentLang()} = $source->name;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->{'name_' . LangService::getCurrentLang()}
        ];
    }
}