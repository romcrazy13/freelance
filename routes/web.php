<?php

use Pecee\SimpleRouter\SimpleRouter as Router;
use App\Controllers\MainController;
use App\Controllers\ApiController;

Router::get('/', [MainController::class, 'mainPage']);

Router::post('/skills', [ApiController::class, 'getSkills']);
Router::post('/projects', [ApiController::class, 'getProjects']);
